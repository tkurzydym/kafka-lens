package dev.tkurzydym.kafkalens.application.producer

import com.fasterxml.jackson.databind.ObjectMapper
import dev.tkurzydym.kafkalens.boundary.producer.CloudEventDTO
import io.cloudevents.CloudEvent
import io.cloudevents.core.v1.CloudEventBuilder
import io.cloudevents.jackson.JsonCloudEventData
import org.apache.kafka.clients.producer.ProducerRecord
import org.apache.kafka.common.header.internals.RecordHeader
import java.net.URI
import java.time.ZoneOffset
import java.util.UUID
import javax.enterprise.context.ApplicationScoped
import javax.inject.Inject
import javax.ws.rs.core.MediaType

const val CONTENT_TYPE = "content-type"
const val MEDIA_TYPE_CE_JSON ="application/cloudevents+json"

@ApplicationScoped
class KafkaProducerApplicationService

@Inject constructor(
    private val objectMapper: ObjectMapper,
) {

    fun convertDataToCloudEventProducerRecord(
        topic: String,
        cloudEventDTO: CloudEventDTO,
    ): ProducerRecord<String, String> {
        val cloudEvent = mapDTOToCloudEvent(cloudEventDTO, topic)

        val cloudEventAsString = objectMapper.writeValueAsString(cloudEvent)

        val producerRecord = ProducerRecord<String, String>(topic, "kafka-lens", cloudEventAsString)

        producerRecord.headers().add(
            RecordHeader(
                CONTENT_TYPE,
                MEDIA_TYPE_CE_JSON.toByteArray(Charsets.UTF_8),
            )
        )

        return producerRecord
    }

    private fun mapDTOToCloudEvent(
        cloudEventDTO: CloudEventDTO,
        topic: String,
    ): CloudEvent? {
        val jsonEventData = JsonCloudEventData.wrap(objectMapper.readTree(cloudEventDTO.data))

        return CloudEventBuilder()
            .withId(UUID.randomUUID().toString())
            .withType(cloudEventDTO.type)
            .withSubject(topic)
            .withSource(URI.create(cloudEventDTO.source!!))
            .withTime(cloudEventDTO.time?.withOffsetSameInstant(ZoneOffset.UTC))
            .withDataContentType(MediaType.APPLICATION_JSON)
            .withData(jsonEventData)
            .build()
    }
}
