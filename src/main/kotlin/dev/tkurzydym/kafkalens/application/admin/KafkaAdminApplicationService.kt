package dev.tkurzydym.kafkalens.application.admin

import dev.tkurzydym.kafkalens.boundary.admin.TopicDescriptionDTO
import org.apache.kafka.clients.admin.AdminClient
import org.apache.kafka.clients.admin.AdminClientConfig
import org.apache.kafka.clients.admin.TopicListing
import org.eclipse.microprofile.config.inject.ConfigProperty
import java.util.Properties
import javax.enterprise.context.ApplicationScoped

@ApplicationScoped
class KafkaAdminApplicationService {
    @ConfigProperty(name = "kafka.bootstrap.servers")
    lateinit var kafkaServer: String

    fun getAllAvailableTopicDescriptions(): List<TopicDescriptionDTO> {

        val adminClient = createAdminClient()

        val topicList: Collection<TopicListing> = adminClient.listTopics().listings().get()

        val topicNames = topicList.map {
            it.name()
        }

        val describeTopics = adminClient.describeTopics(topicNames).all().get().values

        return describeTopics.map {
            TopicDescriptionDTO(it.name(), it.partitions().size)
        }
    }

    fun createAdminClient(): AdminClient {
        val kafkaProperties = Properties()
        kafkaProperties[AdminClientConfig.BOOTSTRAP_SERVERS_CONFIG] = kafkaServer

        return AdminClient.create(kafkaProperties)
    }
}