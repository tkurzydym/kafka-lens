package dev.tkurzydym.kafkalens.application.consumer

import dev.tkurzydym.kafkalens.application.admin.KafkaAdminApplicationService
import org.apache.kafka.clients.consumer.KafkaConsumer
import org.apache.kafka.common.TopicPartition
import org.eclipse.microprofile.config.inject.ConfigProperty
import java.util.Properties
import javax.enterprise.context.ApplicationScoped
import javax.inject.Inject

@ApplicationScoped
class KafkaConsumerApplicationService

@Inject constructor(
    val kafkaAdminApplicationService: KafkaAdminApplicationService,
) {

    @ConfigProperty(name = "kafka.bootstrap.servers")
    lateinit var kafkaServer: String

    fun createKafkaConsumer(
        topic: String,
        consumeCloudEvent: Boolean,
    ): KafkaConsumer<String, String> {
        val props = getConsumerProperties()
        props["group.id"] = "kafka-lens-topic-consumer"

        if (consumeCloudEvent) {
            props["value.deserializer"] = "io.cloudevents.kafka.CloudEventDeserializer"
        } else {
            props["value.deserializer"] = "org.apache.kafka.common.serialization.StringDeserializer"
        }

        val consumer = KafkaConsumer<String, String>(props)
        consumer.subscribe(listOf(topic))

        return consumer
    }

    fun createAllTopicsConsumer(): KafkaConsumer<String, String>  {
        val props = getConsumerProperties()
        props["value.deserializer"] = "org.apache.kafka.common.serialization.StringDeserializer"
        props["group.id"] = "kafka-lens-traffic"

        val kafkaAdminClient = kafkaAdminApplicationService.createAdminClient()

        val topicNames = kafkaAdminClient.listTopics().names().get()


        val consumer = KafkaConsumer<String, String>(props)

        val map = topicNames
            .map { consumer.partitionsFor(it) }
            .flatten()
            .map { TopicPartition(it.topic(), it.partition()) }

        consumer.assign(map)
        consumer.seekToBeginning(map)


        return consumer
    }

    private fun getConsumerProperties(): Properties {
        val props = Properties()
        props["bootstrap.servers"] = kafkaServer
        props["enable.auto.commit"] = "false"
        props["auto.offset.reset"] = "earliest"
        props["key.deserializer"] = "org.apache.kafka.common.serialization.StringDeserializer"
        return props
    }
}
