package dev.tkurzydym.kafkalens.config

import com.fasterxml.jackson.databind.DeserializationFeature
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.SerializationFeature
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule
import com.fasterxml.jackson.module.kotlin.KotlinModule
import io.cloudevents.jackson.JsonFormat
import javax.enterprise.context.ApplicationScoped
import javax.inject.Singleton
import javax.ws.rs.Produces

@ApplicationScoped
class CustomObjectMapperConfig {

    @Singleton
    @Produces
    fun objectMapper(): ObjectMapper =
        ObjectMapper()
            .registerModule(JsonFormat.getCloudEventJacksonModule())
            .registerModule(KotlinModule())
            .registerModule(JavaTimeModule())
            .configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false)
            .configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
}
