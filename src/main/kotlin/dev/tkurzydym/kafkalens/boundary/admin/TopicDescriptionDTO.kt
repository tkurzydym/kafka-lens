package dev.tkurzydym.kafkalens.boundary.admin

data class TopicDescriptionDTO(val topicName: String, val numberOfPartitions: Int)