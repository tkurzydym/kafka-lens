package dev.tkurzydym.kafkalens.boundary.admin

import dev.tkurzydym.kafkalens.application.admin.KafkaAdminApplicationService
import org.eclipse.microprofile.openapi.annotations.Operation
import javax.inject.Inject
import javax.ws.rs.GET
import javax.ws.rs.Path
import javax.ws.rs.Produces
import javax.ws.rs.core.MediaType

@Path("/admin")
class KafkaAdminResource

@Inject constructor(
    val applicationService: KafkaAdminApplicationService
) {
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/topics")
    @Operation(
        operationId = "getTopics",
        summary = "Retrieve all topics",
        description = "Returns all topics that are registered on the connected broker",
    )
    fun getTopics(): List<TopicDescriptionDTO> {
        return applicationService.getAllAvailableTopicDescriptions()
    }
}
