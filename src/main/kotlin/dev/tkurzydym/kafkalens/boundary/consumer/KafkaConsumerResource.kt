package dev.tkurzydym.kafkalens.boundary.consumer

import com.fasterxml.jackson.databind.ObjectMapper
import dev.tkurzydym.kafkalens.application.consumer.KafkaConsumerApplicationService
import io.cloudevents.CloudEvent
import io.cloudevents.kafka.KafkaMessageFactory
import io.cloudevents.rw.CloudEventRWException
import org.apache.kafka.clients.consumer.ConsumerRecord
import org.eclipse.microprofile.openapi.annotations.Operation
import org.jboss.resteasy.annotations.SseElementType
import java.time.Duration
import javax.inject.Inject
import javax.ws.rs.DefaultValue
import javax.ws.rs.GET
import javax.ws.rs.Path
import javax.ws.rs.PathParam
import javax.ws.rs.Produces
import javax.ws.rs.QueryParam
import javax.ws.rs.core.Context
import javax.ws.rs.core.MediaType
import javax.ws.rs.sse.Sse
import javax.ws.rs.sse.SseBroadcaster
import javax.ws.rs.sse.SseEventSink

@Path("/consumer")
class KafkaConsumerResource

@Inject constructor(
    private val applicationService: KafkaConsumerApplicationService,
    private val objectMapper: ObjectMapper
) {
    var broadcaster: SseBroadcaster? = null

    var broadcaster2: SseBroadcaster? = null

    @GET
    @Path("/traffic")
    @Produces(MediaType.SERVER_SENT_EVENTS)
    @SseElementType("text/plain")
    @Operation(
        operationId = "consumeAllTopics",
        summary = "Consumes the Events of all Topics",
        description = "Returns the events of the topics in the system that was provided via SSE. ",
    )
    fun consumeAllTopics(
        @Context sse: Sse,
        @Context sink: SseEventSink
    ) {
        this.broadcaster2 = this.broadcaster2 ?: sse.newBroadcaster()
        broadcaster2?.register(sink)

        val consumer = applicationService.createAllTopicsConsumer()

        while (!sink.isClosed) {
            val poll = consumer.poll(Duration.ofSeconds(5))

            poll.forEach {

                val event = sse.newEventBuilder()
                    .name("allTopics2")
                    .data(objectMapper.writeValueAsString(createTrafficEvent(it)))
                    .mediaType(MediaType.APPLICATION_JSON_TYPE)
                    .build()

                broadcaster2?.broadcast(event)
            }
        }
    }

    private fun createTrafficEvent(kafkaRecord: ConsumerRecord<String, String>): TrafficEvent {

        val event: CloudEvent? = try {
            val reader = KafkaMessageFactory.createReader(kafkaRecord.headers(), kafkaRecord.value().toByteArray())

            reader.toEvent()
        }
        catch (e: CloudEventRWException) {
            null
        }

        return TrafficEvent(kafkaRecord.topic(), kafkaRecord.key(), event?.type, event?.time)
    }

    @GET
    @Path("/{topic}")
    @Produces(MediaType.SERVER_SENT_EVENTS)
    @SseElementType("text/plain")
    @Operation(
        operationId = "consumeTopic",
        summary = "Consumes the Events of a Topic",
        description = "Returns the events of the topic that was provided via SSE. "
                    + "The Service can consume raw payloads or events in a cloud event format. "
                    + "The Default behaviour is to consume cloud event payloads. ",
    )
    fun consumeTopic(
        @PathParam("topic") topic: String,
        @DefaultValue("true") @QueryParam("consumeCloudEvent") consumeCloudEvent: Boolean,
        @Context sink: SseEventSink,
        @Context sse: Sse
    ) {
        this.broadcaster = this.broadcaster ?: sse.newBroadcaster()
        broadcaster?.register(sink)

        val consumer = applicationService.createKafkaConsumer(
            topic,
            consumeCloudEvent,
        )

        while (!sink.isClosed) {
            val poll = consumer.poll(Duration.ofSeconds(5))

            poll.forEach {
                val event = sse.newEventBuilder()
                    .name(topic)
                    .data(it)
                    .mediaType(MediaType.APPLICATION_JSON_TYPE)
                    .build()

                broadcaster?.broadcast(event)
            }
        }
    }
}
