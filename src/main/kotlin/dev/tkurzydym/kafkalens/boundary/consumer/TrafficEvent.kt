package dev.tkurzydym.kafkalens.boundary.consumer

import java.time.OffsetDateTime

data class TrafficEvent(val topic: String, val key: String?, val type: String?, val time: OffsetDateTime?)