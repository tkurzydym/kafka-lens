package dev.tkurzydym.kafkalens.boundary.producer

import java.time.OffsetDateTime

data class CloudEventDTO (
    val type: String?,
    val source: String?,
    val time: OffsetDateTime?,
    val data: String,
)
