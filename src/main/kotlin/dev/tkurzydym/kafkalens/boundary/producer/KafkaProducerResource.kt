package dev.tkurzydym.kafkalens.boundary.producer

import dev.tkurzydym.kafkalens.application.producer.KafkaProducerApplicationService
import org.apache.kafka.clients.producer.KafkaProducer
import org.apache.kafka.clients.producer.ProducerRecord
import org.eclipse.microprofile.config.inject.ConfigProperty
import org.eclipse.microprofile.openapi.annotations.Operation
import java.util.Properties
import javax.enterprise.context.ApplicationScoped
import javax.inject.Inject
import javax.ws.rs.Consumes
import javax.ws.rs.DefaultValue
import javax.ws.rs.POST
import javax.ws.rs.Path
import javax.ws.rs.PathParam
import javax.ws.rs.QueryParam
import javax.ws.rs.core.MediaType
import javax.ws.rs.core.Response

@Path("/producer")
@ApplicationScoped
class KafkaProducerResource

@Inject constructor(
    private val applicationService: KafkaProducerApplicationService,
) {

    @ConfigProperty(name = "kafka.bootstrap.servers")
    lateinit var kafkaServer: String

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/{topic}")
    @Operation(
        operationId = "produceEvent",
        summary = "Produces an Event to the specified Topic",
        description = "This methods produces an Event to the specified topic either in a "
            + "Cloud Events Format or as raw payload, decided by the wrapAsCloudEvent query param. "
            + "The default behaviour is to produce a Cloud Event",
    )
    fun produceEvent(
        @PathParam("topic") topic: String,
        @DefaultValue("true") @QueryParam("wrapAsCloudEvent") wrapAsCloudEvent: Boolean,
        value: CloudEventDTO
    ): Response {
        val props = Properties()
        props["bootstrap.servers"] = kafkaServer
        props["acks"] = "all"
        props["key.serializer"] = "org.apache.kafka.common.serialization.StringSerializer"
        props["value.serializer"] = "org.apache.kafka.common.serialization.StringSerializer"

        val producerRecord = if(wrapAsCloudEvent) {
            applicationService.convertDataToCloudEventProducerRecord(
                topic,
                value,
            )
        } else {
            ProducerRecord(
                topic,
                "kafka-lens",
                value.data,
            )
        }

        val producer = KafkaProducer<String, String>(props)
        producer.send(producerRecord)

        producer.flush()
        producer.close()

        return Response.ok().build()
    }
}
