package dev.tkurzydym.hello

import io.quarkus.test.junit.QuarkusTest
import io.restassured.RestAssured.given
import org.hamcrest.CoreMatchers.`is`
import org.junit.jupiter.api.Test

@QuarkusTest
class HelloKotlinTest {

    @Test
    fun testHelloKotlin() {
        given()
        .`when`()
            .get("/hello/kotlin")
        .then()
            .statusCode(200)
            .body(`is`("Hello Kotlin"))
    }
}
