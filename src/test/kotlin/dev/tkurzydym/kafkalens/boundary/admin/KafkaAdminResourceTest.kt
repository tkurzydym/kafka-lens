package dev.tkurzydym.kafkalens.boundary.admin

import io.quarkus.test.junit.QuarkusTest
import io.restassured.RestAssured
import org.hamcrest.CoreMatchers.`is`
import org.junit.jupiter.api.Test

@QuarkusTest
class KafkaAdminResourceTest {

    @Test
    fun testKafkaAdminGetTopics() {
        RestAssured.given()
            .`when`()
                .get("/admin/topics")
            .then()
                .body(`is`("[{" +
                    "\"topicName\":\"test-topic\"," +
                    "\"numberOfPartitions\":1" +
                    "}]"))
    }
}